//
//  ViewController.m
//  Sample
//
//  Created by Savana on 07/07/15.
//  Copyright (c) 2015 WTS. All rights reserved.
//

#import "ViewController.h"
#import "Recipe.h"
#import "RecipeCell+DisplayRecipe.h"
#import "RecipeDataSource.h"

@interface ViewController ()<UITableViewDelegate>
@property (strong,nonatomic) UITableView *recipeTables;
@property (strong,nonatomic) NSMutableArray *recipes;
@property (strong,nonatomic) RecipeDataSource *rdc;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _recipes =[NSMutableArray new];
    for (int i =0;i<100;i++) {
        Recipe *r =[Recipe new];
        r.title=[NSString stringWithFormat:@"Title %d",i];
        r.details=@"This is a test recipe details of a recipe item to have taste for";
        [_recipes addObject:r];
        
    }
    // Configuring the Cell to be displayed
    TableViewCellConfigureBlock config= ^(RecipeCell *cell,Recipe *item){
        // Which valiues need to be displaued
//        [cell displayDetailsOf:item];
        [cell displayRandomDetailsOf:item];
        
    };
    
    _recipeTables =[[UITableView alloc]  initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height-60.0f)];
    [self.view addSubview:_recipeTables];
    // creating DataSource
    _rdc =[[RecipeDataSource alloc]  initWith:_recipes With:@"recioe" And:config];
    
    /*
     Another way to set the data source
    _rdc =[[RecipeDataSource alloc]  initWith:_recipes With:@"recioe" And:^(RecipeCell *cell, Recipe *item) {
        [cell displayDetailsOf:item];
    }];
     */
  RecipeDataSource *d =  [[RecipeDataSource alloc]  initWith:[NSMutableArray new] With:@"" And:^(id cell, id item) {
        
    }];
    _recipeTables.dataSource=d;
    [_recipeTables registerNib:[UINib nibWithNibName:@"RecipeCell" bundle:nil] forCellReuseIdentifier:@"recioe"];
    
    // Setting the datasource to tableview
    _recipeTables.dataSource=_rdc;
    _recipeTables.delegate = self;
    [self textFieldRight];
    [self setNeedsStatusBarAppearanceUpdate];
    
}
 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}
-(void) textFieldRight
{
    UITextField *tf =[[UITextField alloc]  initWithFrame:CGRectMake(0, 0, 320, 60)];
    [tf setBorderStyle:UITextBorderStyleLine];
    UIView *tv =[[UIView alloc]  initWithFrame:CGRectMake(0, 0, 120, 30)];
    [tv setBackgroundColor:[UIColor redColor]];
    [tf setRightView:tv];
    [tf setRightViewMode:UITextFieldViewModeAlways];
    UIView *tv1 =[[UIView alloc]  initWithFrame:CGRectMake(0, 0, 120, 30)];
    [tv1 setBackgroundColor:[UIColor redColor]];
//    [tv setFrame:CGRectMake(0, 0, 30, 30)];
    [tf setLeftView:tv1];
    [tf setLeftViewMode:UITextFieldViewModeUnlessEditing];
    [self.view addSubview:tf];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
