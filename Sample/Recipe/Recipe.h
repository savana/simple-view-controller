//
//  Recipe.h
//  Sample
//
//  Created by Savana on 07/07/15.
//  Copyright (c) 2015 WTS. All rights reserved.
//

#import <Foundation/Foundation.h>
//! Contains recipe details
@interface Recipe : NSObject
//! Title of recipe
@property (strong,nonatomic) NSString *title;
//! Details of recipe
@property (strong,nonatomic) NSString *details;

@end
