//
//  RecipeCell.h
//  Sample
//
//  Created by Savana on 07/07/15.
//  Copyright (c) 2015 WTS. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface RecipeCell : UITableViewCell
//! UILabvel of recipe title
@property (weak, nonatomic) IBOutlet UILabel *title;
//! UILabel of recipe date
@property (weak, nonatomic) IBOutlet UILabel *date;
//! UILabel of receipe details
@property (weak, nonatomic) IBOutlet UILabel *details;
@end
