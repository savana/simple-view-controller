//
//  RecipeDataSource.h
//  Sample
//
//  Created by Savana on 07/07/15.
//  Copyright (c) 2015 WTS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//! Configure TableViewCell with any celltype and object type
typedef void (^TableViewCellConfigureBlock)(id cell, id item);

@interface RecipeDataSource : NSObject<UITableViewDataSource>
/**
 *  Set up the data source of the table view with the required Details, Identifier and Configuration block
 *
 *  @param items          number of items to be displayed in the tableview
 *  @param cellId         cellIdentier to the tableviewcell which is being displayed in UITableView
 *  @param configureBlock configureBlock for displaying cell
 *
 *  @return self
 */
- (id)initWith:(NSArray *) items With:(NSString *) cellId And:(TableViewCellConfigureBlock )configureBlock __attribute__((nonnull()));
/**
 *  Return a particular item at tha index in the list of items.
 *
 *  @param indexPath current inddexPath of the cell
 *
 *  @return object at a paricular row 
 */
- (id) itemAtindexPath:(NSIndexPath *)indexPath  __attribute__((nonnull()));
@end
