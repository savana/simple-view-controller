//
//  RecipeDataSource.m
//  Sample
//
//  Created by Savana on 07/07/15.
//  Copyright (c) 2015 WTS. All rights reserved.
//

#import "RecipeDataSource.h"

@interface RecipeDataSource()
//! total list of items
@property (strong,nonatomic) NSMutableArray *allRecipes;
//! cell identifier of the UITableViewCell
@property (strong,nonatomic) NSString *cellId;
//! TableViewCell block should be always strong else will result in crash of application
@property (strong,nonatomic) TableViewCellConfigureBlock configBlock;
@end

@implementation RecipeDataSource
#pragma mark - Initialise Datasource
- (id)initWith:(NSArray *)items With:(NSString *)cellId And:(TableViewCellConfigureBlock)configureBlock
{
    self=[super init];
    if(self){
        _allRecipes =[NSMutableArray arrayWithArray:items];
        _cellId= cellId;
        _configBlock = [configureBlock copy];
    }
    
    return self;
}
- (id)itemAtindexPath:(NSIndexPath *)indexPath
{
    return [_allRecipes objectAtIndex:indexPath.row];
}
#pragma mark - UITableViewDataSource delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _allRecipes.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:_cellId];
    id item =[self itemAtindexPath:indexPath];
    _configBlock(cell,item);
    return cell;
}

@end
