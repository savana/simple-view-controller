//
//  RecipeCell+DisplayRecipe.h
//  Sample
//
//  Created by Savana on 07/07/15.
//  Copyright (c) 2015 WTS. All rights reserved.
//

#import "RecipeCell.h"
#import "Recipe.h"

@class  Recipe;
@interface RecipeCell (DisplayRecipe)
/**
 *  Set the values of RecipeCell with the values present in Recipe
 *
 *  @param recipe Recipe details
 */
-(void) displayDetailsOf:(Recipe *)recipe __attribute__((nonnull));
/**
 *  Set the values of RecipeCell with the values presetn in Recipe with the date being set to Current Date & time
 *
 *  @param recipe Recipe deatils
 */
-(void) displayRandomDetailsOf:(Recipe  *)recipe __attribute__((nonnull));
@end