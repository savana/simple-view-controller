//
//  RecipeCell+DisplayRecipe.m
//  Sample
//
//  Created by Savana on 07/07/15.
//  Copyright (c) 2015 WTS. All rights reserved.
//

#import "RecipeCell+DisplayRecipe.h"

@implementation RecipeCell (DisplayRecipe)
- (void)displayDetailsOf:(Recipe *)recipe
{
    self.title.text = recipe.title;
    self.details.text = recipe.details;
    self.date.text = @"";
    
}
- (void)displayRandomDetailsOf:(Recipe *)recipe
{
    self.title.text= recipe.title;
    self.details.text= recipe.details;
    NSDateFormatter *dateFormatter =[NSDateFormatter new];
    [dateFormatter setDateFormat:@"hh:mm:ss"];
    self.date.text= [dateFormatter stringFromDate:[NSDate date]];
    
}
@end
